# Working with webhooks

In this demo we are going to write an Atlassian Connect add-on in Java that can listed to issue created and updated
webhooks and will simply log the webhook messages to the command line. This is just a starting point. In a later demo
we will react to the webhook events by making a change in JIRA. The purpose of this demo is to:

 - Introduce you to the Atlassian Connect Spring Boot Framework (Java)
 - Guide you through using ngrok to proxy your locally running add-on into a cloud instance
 - Show you how easy it is to listen to webhooks in your add-ons

This guide has a number of pre-requisites that you need to have installed on
your machine:

 - Java 8 SDK
 - maven
 - ngrok

Please make sure that you have those installed on your machine before attempting
this tutorial.

## Step 1 - Using maven to generate the base add-on

The [Atlassian Connect Spring Boot Framework][1] is a library that you will use in your Spring Boot applications to
provide the Atlassian Connect add-on functionality to your app. This framework also comes with its own maven archetype.
To use the maven archetype just run the following:

    mvn archetype:generate -DarchetypeGroupId=com.atlassian.connect -DarchetypeArtifactId=atlassian-connect-spring-boot-archetype -DarchetypeVersion=0.9.0-alpha

You will be asked a number of questions, answer as follows:

 - Group Id: com.atlascamp.demo
 - Artifact Id: webhooks-and-requests
 - Version: (hit enter for the default)
 - Package: (hit enter for the default)
 - Y: Y

This will create a new directory called `webhooks-and-requests` in your current directory. This is your new Atlassian Connect
add-on.

## Step 2 - Changing the add-on name

We are now going to change the user facing name of your new add-on. To do this we need to edit your
Atlassian Connect descriptor file.  Open the `webhooks-and-requests` directory in your favourite editor of choice 
and then open the file at: src/main/resources/atlassian-connect.json

In this there will be a property called add-on.name. Change this property to be whatever name you please for your add-on.
This is an example of the name you might choose:

      "name": "AtlasCamp 2016 Addon Webhook and Request example",

Now, when we install your add-on, you will be able to find it easily.

## Step 3 - Running your add-on

To start your application you can type in the following at a terminal, from within the `webhooks-and-requests` directory:

    mvn spring-boot:run

Once the application has started up then you should be able to navigate to http://localhost:8080 in your web browser
and be redirected to your atlassian connect descriptor.

## Step 4 - Using ngrok to access your add-on on the public internet.

For this section [read the Atlassian Connect documentation on developing locally][2].

When you have ngrok installed you should be able to run a command that looks like:

    ngrok http 8080

And expose your add-on to the public internet. You should be able to access your add-on in your web browser via the
public HTTPS url that ngrok provides.

## Step 5 - Installing the add-on in your cloud development instance

This will first require that your atlassian connect descriptor has a base URL that can be accessed over the public internet.
Open the src/main/resources/atlassian-connect.json file and update the `baseUrl` so that it points to the HTTPS URL
provided by ngrok. The Atlassian Connect framework will use that base URL for all requests from the Atlassian Product
back to your application.

To install your add-on in your development instance follow the same instructions that you followed when you
signed up for your development environment: http://go.atlassian.com/about-cloud-dev-instance

In short the steps are:

 1. Copy the public HTTPS URL to your descriptor.
 1. Go to Manage add-ons in JIRA.
 1. Ensure that "Enable development mode" is checked in the Settitngs.
 1. Click "Upload add-on", paste in the descriptor URL and click Upload.

You should see your add-on successfully install into JIRA.

## Step 6 - Updating the descriptor to support webhooks

In the last part of this tutorial we want to register that our add-on should listen to webhook events. We wish to listen
to issue created and issue updated events so the first step is to add a new segment to our Atlassian Connect descriptor
that defines the webhooks. Update the src/main/resources/atlassian-connect.json file to include the following webhook
module definition:

    {
      ...
      "modules": {
        "webhooks": [
          {
            "event": "jira:issue_updated",
            "url": "/webhook/issue"
          },
          {
            "event": "jira:issue_created",
            "url": "/webhook/issue"
          }
        ]
      }
      ...
    }

This declares that you wish to listen to the `jira:issue_updated` and `jira:issue_created` events that are fired from
the Atlassian product. The results of those events will be POSTed to the `/webhook/issue` URL in your add-on. This
means that we now need to register a handler for that resource so that we can react to these events.

Now that you have updated your atlassian connect descriptor you will need recompile your add-on so that the changes are
redeployed locally:

    mvn compile

Then you will need to re-install your add-on into your cloud development instance. Go and give that a try now. When you
try you should get an error that reads:

> Installation failed. The add-on defines features that require the READ scope, but does not request the READ scope
> in its descriptor.

When your add-on asks to register a webhook is is asking to read a stream of events from the Atlassian Product. Therefore
you also need to add a scopes block that declares read scope to your descriptor:

    {
      ...
      "scopes": ["read"],
      ...
    }

Recompile your add-on again and install it in your cloud development instance and the installation should succeed.
Congratulations, JIRA will now start posting webhook events back to your add-on.

## Step 7 - Writing a webhook listener

Start by creating a Java class called WebhookResource next to the AddonApplication class.

Then modify the class with the following:

 - Add a @Controller annotation to the class because it should be registered in spring.
 - Create a SLF4J logger so that you can write the contents of your webhook body to the logs (console).

You should end up with some code that looks like this:

    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    import org.springframework.stereotype.Controller;
    
    @Controller
    public class WebhookResource
    {
        private static final Logger log = LoggerFactory.getLogger(WebhookResource.class);

    }

The webhook data is sent to us in JSON format so we will need the [Jackson library][3] in order to parse it. This means
 that you will need to open your pom.xml and include the Jackson library to your dependencies:

    <dependency>
        <groupId>com.fasterxml.jackson.core</groupId>
        <artifactId>jackson-core</artifactId>
        <version>2.7.4</version>
    </dependency>

Now it is time to add our first resource mapping to handle the `/webhook/issue` webhook event. It should look like this:

    @RequestMapping(value = "/webhook/issue", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void issueCreatedOrUpdated(AtlassianHostUser atlassianHostUser, @RequestBody JsonNode webhookEvent) {
        final AtlassianHost atlassianHost = atlassianHostUser.getHost();
        log.info("Webhook host: {} ({})", atlassianHost.getBaseUrl(), atlassianHost.getClientKey());
        log.info("Webhook user: " + atlassianHostUser.getUserKey());

        final String issueKey = webhookEvent.get("issue").get("key").textValue();
        log.info("Issue key is: " + issueKey);
        log.info("All webhook data: " + webhookEvent);
    }

Lets break down this code line by line. In the annotation we are declaring that the following method will handle a
HTTP request. The value and method fields combined specifies that this method will handle any POST requests to
`/webhook/issue`.

On the next line we declare that the method will not return a result (which ends up returning a HTTP 204). We also request
an AtlassianHostUser and this is where the Atlassian Connect Spring Boot framework really helps us out; it will automatically
assume that every resource in the application requires JWT authentication and, after parsing the JWT token, will make the
AtlassianHostUser object available. This object contains the Atlassian Host that made the request and the User that made
the request, should such a user exist. This is really handy in handling authorised requests to this resource. The final
part of this method is mapping the body of the request to the `webhookEvent` variable that is backed by a JsonNode. This
will automatically parse JSON data in the webhook POST body into a Jackson JsonNode object which we can reference in the
rest of the code.

The next three lines print the atlassian host and users details to the logs. The following three lines after that log the
webhook event data. From here we are done and now we should test our code.

From the command line, recompile the code so that your changes are redeployed locally:

    mvn compile

Then move on to the next step.

## Step 7 - Testing our code

We want to test our code so first lets test the happy path:

 1. You have modified your add-on descriptor. If, and only if, you change your add-on descriptor you will need to re-install
    your add-on into your cloud development to see the changes reflected in the Atlassian product. Go to the Manage add-ons
    and re-upload your add-on again. You do NOT need to uninstall it first; you can just re-install it over the top of the
    previous add-on.
 1. In JIRA create a new issue.
 1. Look at the logs and see the details of the jira:issue_created webhook.

When you do that your logs should look something like this:

    2016-05-14 18:42:20.764  INFO 79323 --- [nio-8080-exec-7] c.a.c.s.c.a.j.JwtAuthenticationProvider  : Authenticated token for host: jira:8aa289a9-406c-44d9-ab5b-7fbb53452d24
    2016-05-14 18:42:20.774  INFO 79323 --- [nio-8080-exec-7] c.a.demo.example.WebhookResource         : Webhook host: https://rmassaioli-development.atlassian.net (jira:8aa289a9-406c-44d9-ab5b-7fbb53452d24)
    2016-05-14 18:42:20.774  INFO 79323 --- [nio-8080-exec-7] c.a.demo.example.WebhookResource         : Webhook user: Optional[admin]
    2016-05-14 18:42:20.774  INFO 79323 --- [nio-8080-exec-7] c.a.demo.example.WebhookResource         : Issue key is: TP-24
    2016-05-14 18:42:20.774  INFO 79323 --- [nio-8080-exec-7] c.a.demo.example.WebhookResource         : All webhook data: {"timestamp":1463215339446,"webhookEvent":"jira:issue_created","issue_event_type_name":"issue_created","user":{"self":"https://rmassaioli-development.atlassian.net/rest/api/2/user?username=admin","name":"admin","key":"admin","emailAddress":"rmassaioli@atlassian.com","avatarUrls":{"48x48":"https://secure.gravatar.com/avatar/418a091f95a9941cd046268ef9b6f949?d=mm&s=48","24x24":"https://secure.gravatar.com/avatar/418a091f95a9941cd046268ef9b6f949?d=mm&s=24","16x16":"https://secure.gravatar.com/avatar/418a091f95a9941cd046268ef9b6f949?d=mm&s=16","32x32":"https://secure.gravatar.com/avatar/418a091f95a9941cd046268ef9b6f949?d=mm&s=32"},"displayName":"Robert  [Administrator]","active":true,"timeZone":"Australia/Sydney"},"issue":{"id":"10200","self":"https://rmassaioli-development.atlassian.net/rest/api/2/issue/10200","key":"TP-24","fields":{"issuetype":{"self":"https://rmassaioli-development.atlassian.net/rest/api/2/issuetype/10002","id":"10002","description":"A task that needs to be done.","iconUrl":"https://rmassaioli-development.atlassian.net/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype","name":"Task","subtask":false,"avatarId":10318},"timespent":null,"project":{"self":"https://rmassaioli-development.atlassian.net/rest/api/2/project/10000","id":"10000","key":"TP","name":"Test Project","avatarUrls":{"48x48":"https://rmassaioli-development.atlassian.net/secure/projectavatar?avatarId=10324","24x24":"https://rmassaioli-development.atlassian.net/secure/projectavatar?size=small&avatarId=10324","16x16":"https://rmassaioli-development.atlassian.net/secure/projectavatar?size=xsmall&avatarId=10324","32x32":"https://rmassaioli-development.atlassian.net/secure/projectavatar?size=medium&avatarId=10324"}},"fixVersions":[],"aggregatetimespent":null,"resolution":null,"resolutiondate":null,"workratio":-1,"lastViewed":null,"watches":{"self":"https://rmassaioli-development.atlassian.net/rest/api/2/issue/TP-24/watchers","watchCount":0,"isWatching":false},"created":"2016-05-14T18:42:19.260+1000","priority":{"self":"https://rmassaioli-development.atlassian.net/rest/api/2/priority/3","iconUrl":"https://rmassaioli-development.atlassian.net/images/icons/priorities/medium.svg","name":"Medium","id":"3"},"customfield_10023":null,"customfield_10024":[],"customfield_10025":null,"labels":[],"customfield_10026":null,"customfield_10016":"0|i0005b:","customfield_10017":null,"customfield_10018":null,"timeestimate":null,"aggregatetimeoriginalestimate":null,"versions":[],"issuelinks":[],"assignee":null,"updated":"2016-05-14T18:42:19.260+1000","status":{"self":"https://rmassaioli-development.atlassian.net/rest/api/2/status/10000","description":"","iconUrl":"https://rmassaioli-development.atlassian.net/","name":"To Do","id":"10000","statusCategory":{"self":"https://rmassaioli-development.atlassian.net/rest/api/2/statuscategory/2","id":2,"key":"new","colorName":"blue-gray","name":"To Do"}},"components":[],"timeoriginalestimate":null,"description":"test description","customfield_10010":null,"customfield_10011":null,"customfield_10012":null,"customfield_10013":"Not started","customfield_10014":null,"timetracking":{},"customfield_10015":"com.atlassian.servicedesk.plugins.approvals.internal.customfield.ApprovalsCFValue@db252d","customfield_10005":null,"customfield_10006":null,"customfield_10007":null,"customfield_10008":null,"attachment":[],"customfield_10009":null,"aggregatetimeestimate":null,"summary":"Test issue","creator":{"self":"https://rmassaioli-development.atlassian.net/rest/api/2/user?username=admin","name":"admin","key":"admin","emailAddress":"rmassaioli@atlassian.com","avatarUrls":{"48x48":"https://secure.gravatar.com/avatar/418a091f95a9941cd046268ef9b6f949?d=mm&s=48","24x24":"https://secure.gravatar.com/avatar/418a091f95a9941cd046268ef9b6f949?d=mm&s=24","16x16":"https://secure.gravatar.com/avatar/418a091f95a9941cd046268ef9b6f949?d=mm&s=16","32x32":"https://secure.gravatar.com/avatar/418a091f95a9941cd046268ef9b6f949?d=mm&s=32"},"displayName":"Robert  [Administrator]","active":true,"timeZone":"Australia/Sydney"},"subtasks":[],"reporter":{"self":"https://rmassaioli-development.atlassian.net/rest/api/2/user?username=admin","name":"admin","key":"admin","emailAddress":"rmassaioli@atlassian.com","avatarUrls":{"48x48":"https://secure.gravatar.com/avatar/418a091f95a9941cd046268ef9b6f949?d=mm&s=48","24x24":"https://secure.gravatar.com/avatar/418a091f95a9941cd046268ef9b6f949?d=mm&s=24","16x16":"https://secure.gravatar.com/avatar/418a091f95a9941cd046268ef9b6f949?d=mm&s=16","32x32":"https://secure.gravatar.com/avatar/418a091f95a9941cd046268ef9b6f949?d=mm&s=32"},"displayName":"Robert  [Administrator]","active":true,"timeZone":"Australia/Sydney"},"customfield_10000":null,"aggregateprogress":{"progress":0,"total":0},"customfield_10001":null,"customfield_10002":null,"customfield_10003":null,"customfield_10004":null,"environment":null,"duedate":null,"progress":{"progress":0,"total":0},"comment":{"startAt":0,"maxResults":0,"total":0,"comments":[]},"votes":{"self":"https://rmassaioli-development.atlassian.net/rest/api/2/issue/TP-24/votes","votes":0,"hasVoted":false},"worklog":{"startAt":0,"maxResults":20,"total":0,"worklogs":[]}}}}

Congratulations! Your add-on is now receiving webhook events; you can react to these webhook events in any way that you
please.

Our next step is to see what would happen if an arbitrary attacker attempted to post data to your `/webhook/issue` resource.
Maybe they are trying to get you to accept fake information and do something that you would otherwise not do. We can
simulate this by posting the following rest call:

    curl -X POST "http://localhost:8080/webhook/issue"

If you have done everthing correctly then you should recieve a 401 Unauthorized in that callback. This is because you
have not provided a valid JWT token with the request (because you are not the Atlassian product that is authorized to
make such requests) and the framework automatically protects its URL's from missing JWT tokens. If you wanted to have
a url that was not protected (as in, it did not require a JWT token) then you could use the @IgnoreJWT annotation
on that ResourceMapped Java method.

 [1]: https://bitbucket.org/atlassian/atlassian-connect-spring-boot
 [2]: https://developer.atlassian.com/static/connect/docs/latest/developing/developing-locally-ngrok.html
 [3]: https://github.com/FasterXML/jackson
