# Using the JSAPI

In this guide, we will be creating a static add-on which will serve up two iframes.
The first iframe will be a webpanel, that will be on any Confluence Page, under the comments section.
The second will be a dialog, which will be created by the first webpanel.

The purpose of this demo is to:
 
 1. Introduce you to the Atlassian Connect JSAPI,
 2. Guide you through creating your own dialog, and
 3. How to pass data between iframes.

This guide has a number of pre-requisites that you need to have installed on
your machine:

 - ngrok
 - python 2

We will also require a project space. A new directory or folder should do.

# Step 1 - Creating the base static add-on

Create a new file called `atlassian-connect.json` in the new project directory, and paste in the code below.

```

{
    "key": "atlascamp-conf-tutorial",
    "name": "Static JSAPI Tutorial for Confluence",
    "baseUrl": "https://<ngrok-url>.ngrok.io",
    "authentication": {
        "type": "none"
    },
    "modules": {
        "webPanels": [
            {
                "url": "/webpanel.html",
                "location": "atl.comments.view.bottom",
                "key": "jsapi-webpanel",
                "name": {
                    "value": "JSAPI Tutorial Webpanel"
                },
                "layout": {
                    "height": "100px",
                    "width": "100%"
                }
            }
        ],
        "dialogs": [
            {
                "url": "/dialog.html",
                "key": "jsapi-dialog",
                "options": {
                    "size": "medium",
                    "header": {
                        "value": "JSAPI Tutorial Dialog"
                    },
                    "chrome": true
                }
            }
        ]
    }
}

```

This descriptor defines two modules: one webpanel, and the other a dialog.  
The important details that the webpanel defines:
 
 1. a url to look up the content of the iframe, and
 2. a location in Confluence (this location is under the comments section but for more locations, checkout the [Wittified WebFragment Finder][1]).

The dialog defines:

 1. A url to load the iframe contents from, 
 2. A key to be referenced when creating the dialog, and
 3. Additional options, which changes how our dialog appears on the screen

With both of these modules, there are additional options that have been pre-set to make the JavaScript simpler.

Now we need to make these modules load up html content.

Create a new file in your directory called `webpanel.html` and paste in the following content:

```
<!DOCTYPE html>
<html lang="en">
   <head>
         <!-- jQuery -->
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

      <!-- Atlassian User Interface (AUI) -->
      <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.7.0/css/aui.css" media="all">
      <script src="//aui-cdn.atlassian.com/aui-adg/5.7.0/js/aui.js"></script>
      <script id="connect-loader" data-options="sizeToParent:true;">
        (function() {
          var getUrlParam = function (param) {
            var codedParam = (new RegExp(param + '=([^&]*)')).exec(window.location.search)[1];
            return decodeURIComponent(codedParam);
          };

          var baseUrl = getUrlParam('xdm_e') + getUrlParam('cp');
          var options = document.getElementById('connect-loader').getAttribute('data-options');

          var script = document.createElement("script");
          script.src = baseUrl + '/atlassian-connect/all.js';

          // TODO Used for passing data by customEvent

          if(options) {
            script.setAttribute('data-options', options);
          }

          document.getElementsByTagName("head")[0].appendChild(script);
        })();
      </script>
  </head>
  <body style="background-color:#FFF; padding-top: 10px;">
    <div class="ac-content">
      <h2>Confluence Image Dialog Tutorial</h2>
      <div>
        <input id="url-text" type="url" value="Insert an image url">
      </div>
      <p>
        <button id="create-dialog" class="aui-button aui-button-primary" resolved="">Create Dialog</button>
      </p>
    </div>
    <script type="text/javascript">
    (function(){
      $("#create-dialog").on("click", function(){
        AP.require("dialog", function(dialog){
          var createdDialog = dialog.create({
            "key":"jsapi-dialog"
          });

          // TODO Place JavaScript from Stage 3 here

        });
      });
    })();
    </script>
  </body>
</html>

```

This html blob contains a few dependencies like [AUI](https://docs.atlassian.com/aui/latest/)
to make our web panel look seamless with the Atlassian product, jquery and a script to load the all.js file.

In the body, we have a text field and a button.

At the bottom of the file, we have an event listener tied to our button, that will call a function which will create the dialog when clicked. Notice that the JSON object being passed into the `dialog.create` function only contains `key` which contains the same key from our add-ons descriptor. More options can be passed in here, but they will overwrite anything that has been set in the descriptor.

The text input doesn't do anything currently but will be used in later stages.

Now we need to load something when the button is pressed. Create a new file called `dialog.html` and paste in the following:

```
<!DOCTYPE html>
<html lang="en">
   <head>
         <!-- jQuery -->
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

      <!-- Atlassian User Interface (AUI) -->
      <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.7.0/css/aui.css" media="all">
      <script src="//aui-cdn.atlassian.com/aui-adg/5.7.0/js/aui.js"></script>
      <script id="connect-loader" data-options="sizeToParent:true;">
        (function() {
          var getUrlParam = function (param) {
            var codedParam = (new RegExp(param + '=([^&]*)')).exec(window.location.search)[1];
            return decodeURIComponent(codedParam);
          };

          var baseUrl = getUrlParam('xdm_e') + getUrlParam('cp');
          var options = document.getElementById('connect-loader').getAttribute('data-options');

          var script = document.createElement("script");
          script.src = baseUrl + '/atlassian-connect/all.js';

          // TODO Place JavaScript from Step 3 here

          if(options) {
            script.setAttribute('data-options', options);
          }

          document.getElementsByTagName("head")[0].appendChild(script);
        })();
      </script>
  </head>
  <body style="background-color:#FFF;">
    <div class="ac-content">
      <div>
        <input id="url-input" type="text" value="Insert an image url" style="width:100%">
      </div>
      <p>
        <img id="url-display" src="https://s-media-cache-ak0.pinimg.com/736x/5a/aa/3f/5aaa3f6a391dc621fc8a909ba5ef795d.jpg" style="width:580px;"/>
      </p>
    </div>
    <script type="text/javascript">
      $("#url-input").on("input", function() {
        var newUrl = $("#url-input").val();
        $("#url-display").attr("src", newUrl);
      });
    </script>
  </body>
</html>

```

At the top of "dialog.html", the scripts and style sheets are exactly the same as what is in "webpanel.html".

The body is filled with another text field, and a preview for an image url.

Underneath this is a simple script to check for changes in the input field and apply them to the source image.


# Step 2 - Installing the add-on on Confluence

The initial instructions for setting up the static add-on, are just the same as you have done for the "simple-static-addon-in-bitbucket". 

## Hosting your static add-on

Currently we only have static files but, in order to turn that into a static add-on you need
to serve those files via a HTTP webserver. Here you have a number of options.

### Using node http-server

If you already have npm installed then you can just install [http-server][2] and use that. In your
base directory:

    $ npm install http-server
    $ ln -s node_modules/.bin/http-server .
    $ ./http-server -p 8011

### Using python HttpServer

If you have python installed you can also use that to generate a HTTP Server in your
bitbucket-static base directory. Just do the following:

    $ python -m SimpleHTTPServer 8011 

### Running ngrok

If you have successfully started up a HTTP server on port 8011 then you should be able to navigate to
http://localhost:8011/atlassian-connect.json and see the descriptor that you wrote in the previous
step.

You then need to make this descriptor available on the public internet. Assuming that [you
successfully installed ngrok in the previous tutorial][4] you should just be able to run the following in
another terminal window:

    $ ngrok http 8011

When this works you should receive a HTTPS URL through which you can access
https://<ngrok-url>/atlassian-connect.json. However, when you visit your Atlassian Connect descriptor, 
you will notice that the baseUrl still has not been injected correctly. Replace the 
baseUrl in your descriptor with your newly generated ngrok URL.

## Installing on Confluence

Now that your static add-on is ready to be installed, navigate to your Confluence instance's UPM page.
This is easiest done by going to Confluence, clicking the cog in the top right corner, and clicking "Add-ons".

__Note:__ If you have not done so already, turn on development mode by clicking on "Settings" at the bottom of the page, and checking "Enable development mode".  

On the top right, there is now a link labeled "Upload add-on". Click on this, and paste the ngrok URL that leads to your add-ons descriptor. This is the same link you visited in the last section.  
Click "Upload" to install your add-on.

Check that the add-on has installed by going to any Confluence page, and checking for your content below the comments section.

This web panel will have an input field, and a button. Clicking on the button will create a dialog with another text field, and an image preview. Changes to the text input, will assume a valid url to an image, and attempt to load that in the preview.

# Step 3 - Passing data between iframes

We now have an add-on that creates two iframes which seem to have no communication between them.  
For a standard website which contains iframes, these iframes will not be able to communicate with each other.  
This means, if you wanted one iframe, to talk to the other, you would be required to have a back end service which could send updates from one ifram to another.  

However, the JSAPI, loaded in from the all.js file, allows you to talk between the iframes on the client side, removing the need for a back end service.

## Passing data on dialog initialization

When creating a dialog, additional options can be used. The option we are going to use for this will be the `customData` option.
In your webpanel.html file, go to the `dialog.create` method, and alter the object input so it looks like 

    var createdDialog = dialog.create({
        "key":"jsapi-dialog",
        "customData": $("#url-text").val()
    });

This will get text from the input field, and give it to the dialog on creation.
The dialog will now get this data but we need to tell it what to do. In the `dialog.html` file, where it says "// TODO Place JavaScript from Step 3 here", replace this text with:

    script.onload = function() {
        AP.require("dialog", function(dialog){
            if (dialog.customData) {
                $("#url-display").attr("src", dialog.customData);
                $("#url-input").val(dialog.customData);
            }

            // TODO Used for passing data on closing
        });

        // TODO Used for passing data on customEvent
    };

This bit of code will run when the "all.js" file has loaded, and will check for the `customData` field, before placing this on in the image and text inputs. We are using this method, since we want to be making changes as soon as all.js has loaded. In-lining this JavaScript further down may run into issues since the all.js script will load asynchronously 

What this little bit of code has allowed us to do, is to pass data to the new iframe on construction.

Test it our by entering an image URL on the text input located under the comments section, before creating the dialog.

Sample Image URL: https://upload.wikimedia.org/wikipedia/en/b/be/Red_Panda_in_a_Gingko_tree.jpg

## Passing data on dialog closing

Currently, if we closed the dialog after having changed the url, our changes will not be reflected in the original page.

In "dialog.html", replace "// TODO Used for passing data on closing" with:

    dialog.getButton("submit").bind(function(){
        var result = {};
        result.url = $("url-input").val();
        dialog.close(result);
    });

This code will take control of the "Submit" button, meaning that the dialog could, in theory, block the dialog from closing if the dialog wanted to do validation of the input parameters.
We won't be doing any validation here, but we will create an object containing the url, and close the dialog with this data.

In the "webpanel.html", where it says "// TODO Place JavaScript from Stage 3 here", replace the text with:

    createdDialog.on("close", function(result){
        if(result) {
          $("#url-text").val(result.url);
        }
    });

This creates an event listener on the dialogs "close" event and calls our function when this happens. In this case we will be receiving the result, and passing the key into the text input.  
Note: We may not get a result since we have not overridden the "Cancel" button, so we must test if it is defined.

Test this out by entering a URL in the dialog, before clicking "Submit".

Sample Image URL: https://s-media-cache-ak0.pinimg.com/600x315/7c/41/19/7c4119fedcc4d41c45c2554c83d1b9d3.jpg

## Passing data from dialog immediately

As an optional extra, there is also the events system that we can use to pass data between our add-ons iframes, on an event which we control (ie not tied to dialog events).

Within "dialog.html", replace "// TODO Used for passing data on customEvent" with:

    AP.require("events", function(events) {
        $("#url-input").on("input", function() {
            events.emit("url-change", $("#url-input").val())
        });
    });

This makes the dialog fire a custom event to our iframes, listening to the event name (which in this case is "url-change"), and passes over the value of the input text box.
Now we need something to listen to this event.

In "webpanel.html", replace "// TODO Used for passing data by customEvent" with:

    script.onload = function() {
        AP.require("events", function(events){
            events.on("url-change", function(data) {
                $("#url-text").val(data);
            });
        });
    };

This code creates a subscriber to our "url-change" event, and passes the URL that will be the data, to the text field.

Test this out by opening the dialog, and editing the text inside the input field. You should be able to see the text changing in both input fields as you type.

Sample image URL: http://img.gmw.cn/images/attachement/jpg/site2/20140326/eca86ba05530149d127d04.jpg


# Conclusion

What we have seen from these examples, is an easy way to take advantage of the JSAPI, and be able to share data without a back end service.

There are many more features in the JSAPI that you can look into that can each reduce more overhead in not only development time, creating alternative solutions to these features, but also to reduce the load on your services, especially when it comes to scaling your services.

 [1]: https://marketplace.atlassian.com/plugins/com.wittified.webfragment-finder-confluence/cloud/overview
 [2]: https://www.npmjs.com/package/http-server
