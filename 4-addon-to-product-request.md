# Making a REST call from add-on to product

This guide continues from the previous webhooks tutorial. In the previous add-on
we listened to a webhook event and output the webhook event data that we received
to the logs.

In this guide we are going to update the previous example so that, when we receive
an issue created or updated event we comment on the issue with who made that
change to the issue.

## Step 0 - Make sure that you have the source code

If you did not finish the previous exercise then you have two options:

 - Go back and finish the previous exercise
 - [Checkout this pre-made copy of the source code.][1]

Once you have a fully finished version of the source code you can begin this
guide in earnest. We will be assuming that you have the correct ngrok tunnel
setup, that the addon is running locally (using mvn spring-boot:run) and that
it is installed in your cloud development environment.

## Step 1 - Request a RestTemplate

[As you have learned][2], in order to make a call from your add-on to the
Atlassian product you need to sign it with a valid JWT token. Luckily for you,
the Atlassian Connect Spring Boot framework will generate that token
automatically for you. If you wish to see how that works in detail then you
can [read the relevant source code][3].

However, for you it is as simple as requesting a RestTemplate in Spring Boot
to make a request with. Open up the WebhookResource class from the previous
example and add the following field to the class:

    @Controller
    public class WebhookResource
    {
        @Autowired
        private RestTemplate restTemplate;

This will automatically give us a RestTemplate that can be used to make requests
to the Atlassian product; to JIRA in our case.

## Step 2 - Making a HTTP request with Atlassian Connect Spring Boot

You use the RestTemplate like so:

    // Get the issue key from the webhook event
    final String issueKey = webhookEvent.get("issue").get("key").textValue();

    // Get the user key of the user that caused this webhook event to fire
    final Optional<String> potentialUserKey = atlassianHostUser.getUserKey();

    // Ensure that there is a user that made this request and that this webhook was not fired for a comment
    if(potentialUserKey.isPresent() && !webhookEvent.has("comment")) {
        // Create the JSON representation of our payload
        final ObjectNode payload = JsonNodeFactory.instance.objectNode()
            .put("body", "There was an update to this issue made by: " + potentialUserKey.get());

        // Create the HTTP Entity that we will send to JIRA with our payload and the right headers
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> commentEntity = new HttpEntity<>(payload.toString(), headers);

        // Use the RestTemplate to add a comment to the issue
        // REST API from docs: https://docs.atlassian.com/jira/REST/latest/#api/2/issue-addComment
        restTemplate.postForEntity(atlassianHostUser.getHost().getBaseUrl() + "/rest/api/2/issue/" + issueKey + "/comment", commentEntity, String.class);
    }

Read the comments in this block of code to understand what it does in detail.
However, in summary, we extract the required data from the webhook event and the
AtlassianHostUser object, check to make sure that this webhook was not a comment
and then add our own comment to the issue.

You may simply copy and paste this sample code into the request handler for the
`/webhook/issue` resource in the WebhookResource class to use it in your add-on.

The check to ensure that this webhook event is not for a comment is very important
because it prevents an infinite loop. When you make a comment on the issue then,
from JIRA's perspective, this is another jira:issue_updated event and it will send
another webhook event to your add-on. Which results in another comment POST. Which
results in another webhook event. Which results in another comment POST. And over,
and over it goes. When writing your own add-ons be aware that you may get webhook
events for your own actions.

If you recompile the code and then make a modification to an issue (update the
summary or description) then you can see this code in action. Give that a try now.

## Step 3 - Upgrading your scopes

If you tried the previous step against JIRA you will notice that...nothing happened
to the JIRA issue. No comment was added. If you then look at the logs for your add-on
you will see that you got an error message that looks like this:

    org.springframework.web.client.HttpClientErrorException: 403 Forbidden

Remember, whenever you make a rest call and you get a 403 Forbidden error it is
a good time to check that you have the required scopes. From our last example we
only needed to listen to webhook events, so we only requested 'read' scope. However,
now we are trying to add a comment to an issue; that is a 'write' operation.

Therefore, in order to not get an exception, we need to update our descriptor
to look like so:

    {
      ...
      "scopes": ["read", "write"],
      ...
    }

Because this is a change to your descriptor you will need to recompile the code
and reinstall the descriptor into your Atlassian cloud development environment.

Once you have reinstalled the updated descriptor go back to your issue and edit
it again (maybe edit the summary or description). This time, after the edit is
complete you should be able to refresh the page and see the new comment that
has been placed by your add-on.

Congratulations! You have now made a request from your add-on to an Atlassian product.

## Step 4 - Further excercises

If you have reached this point in time it would be a good idea to try and make some
more rest calls to JIRA. Here are some things that you could try:

 - When you receive the webhook get the email address of the user that updated the issue.
 - Save some data in an entity property against the issue.

You can [look at the latest JIRA REST docs for ideas][4] on the resources that you can
query with your RestTemplate. Good luck and happy developing!

 [1]: https://bitbucket.org/robertmassaioli/webhooks-and-requests
 [2]: https://developer.atlassian.com/static/connect/docs/latest/concepts/authentication.html
 [3]: https://bitbucket.org/atlassianlabs/atlassian-connect-spring-boot/src/a9749a97f90fde4b0c0b23d9c8a2105b212e0592/atlassian-connect-spring-boot-core/src/main/java/com/atlassian/connect/spring/core/request/jwt/JwtSigningClientHttpRequestInterceptor.java?at=master&fileviewer=file-view-default#JwtSigningClientHttpRequestInterceptor.java-103
 [4]: https://docs.atlassian.com/jira/REST/latest/
